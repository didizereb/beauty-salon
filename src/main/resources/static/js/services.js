var mainUrl = "http://localhost:9000";

$(document).ready(function() {
    getAllServices1();
    setModalConfiguration1();
    setActionOnCreateBtn1();
    setActionOnUpdateBtn1();
    getAllCategories();
});

function  getAllServices1() {
    $.ajax({
        url: mainUrl + "/service",
        type: "GET",
        contentType: "application/json",
        success: function (serviceResponse) {
            console.log(serviceResponse);
            setServicesToTable(serviceResponse);
            setActionOnDeleteButtons1();
            setActionOnUpdateButtons1();
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function  getAllCategories() {
    $.ajax({
        url: mainUrl + "/category",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            setCategoriesToRadio(data);
        },
        error: function (error) {
            alert(error.message);
        }
    });
}

function  setCategoriesToRadio(categories) {
    for (var i = 0; i < categories.length; i++) {
        setCategoryToRadio(categories[i]);
    }
}

function  setCategoryToRadio(category) {
    var radioOfCategories = $("#service-category");
    radioOfCategories.append('<p><input name="categoryId" type="radio" value="'+category.id+'"> '+category.name+'</p>');
}

function setServicesToTable(services) {
    for (var i = 0; i < services.length; i++) {
        setServiceToTable(services[i]);
    }
}

function setServiceToTable(service) {
    var tableOfServices = $("#services");
    tableOfServices.append('<tr>' +
        '<td>' + service.name + '</td>' +
        '<td>' + service.price + '</td>' +
        '<td>' +service.categoryName + '</td>' +

        '<td><button class="delete-button-service" value="' + service.id + '">Delete</button></td>' +
        '<td><button class="update-button-service" value="' + service.id + '">Update</button></td>' +
        '</tr>');
}

function setActionOnDeleteButtons1() {
    $(".delete-button-service").each(function (index) {

        $(this).click(function () {

            if (confirm("Are you sure?")) {
                $.ajax({
                    url: mainUrl + "/service" ,
                    type: "DELETE",
                    data: {
                        id:$(this).val()
                    },
                    success: function (data) {
                        location.reload();
                    },
                    error: function (error) {
                        alert("This service is related to a worker");
                    }
                });
            }


        })
    })

}

function setActionOnCreateBtn1() {
    $("#btnCreateService").click(function () {

        var  name= $("#name").val();
        var price = $("#price").val();
        var category = $('input[name="categoryId"]:checked').val();


        if (!name) {
            alert('name can\'t be empty');
            return false;
        }
        if (!price) {
            alert('price  can\'t be empty');
            return false;
        }
        if (!category) {
            alert('Category  can\'t be empty');
            return false;
        }


        var newService = {
            "name" : name,
            "price" : price,
           "categoryId" : category

        };
        console.log(newService);

        $.ajax({
            url: mainUrl + "/service",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(newService),
            success: function (data) {
                location.reload();
            },
            error: function (error) {
                alert(error.message);
            }
        });
    });
}

function setModalConfiguration1() {
    // Get the modal
    var modal = document.getElementById('myModal1');

    // Get the button that opens the modal
    var btn = document.getElementById("openModal1");

    // Get the <span> element that closes the modal
    var span = document.getElementById("closeServises");

    // When the user clicks the button, open the modal
    btn.onclick = function () {
        modal.style.display = "block";
        $('#id1').val('');
        $('#name').val('');
        $('#price').val('');
        $('#service-category').val('');


        $('#btnUpdateService').hide();
        $('#btnCreateService').show();
        $('#title1').text('Create Service');
        $('#footer1').text('Form for new service');
    };

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    };

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}

function setActionOnUpdateButtons1() {
    $(".update-button-service").each(function (index) {
        $(this).click(function () {

            $.ajax({
                url: mainUrl + "/service/one" ,
                type: "GET",
                data: {
                    id:$(this).val()
                },
                success: function (data) {
                    $('#id1').val(data.id);
                    $('#name').val(data.name);
                    $('#price').val(data.price);
                    $('input:radio[name="categoryId"]').filter('[value="'+data.categoryId+'"]').attr('checked', true);


                    $('#myModal1').show();
                    $('#btnUpdateService').show();
                    $('#btnCreateService').hide();
                    $('#title1').text('Update service');
                    $('#footer1').text('Form for update service');
                },
                error: function (error) {
                    alert(error.message);
                }
            });
        })
    })

}

function setActionOnUpdateBtn1() {
    $("#btnUpdateService").click(function () {
        var id = $("#id1").val();
        var  name = $("#name").val();
        var price = $("#price").val();
        var category = $('input[name=categoryId]:checked').val();

        if (!name) {
            alert('Name can\'t be empty');
            return false;
        }
        if (!price) {
            alert('Price  can\'t be empty');
            return false;
        }
        if (!category) {
            alert('Category  can\'t be empty');
            return false;
        }

        var newService = {
            "name" : price,
            "id": id,
            "price": price,
            "categoryId": category


        };

        $.ajax({
            url: mainUrl + "/service?id="+ id,
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(newService),
            success: function (data) {
                location.reload();
            },
            error: function (error) {
                alert(error.message);
            }
        });
    });
}




