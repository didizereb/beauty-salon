package diana.zherebetska.beauty_salon.dto.response;


import diana.zherebetska.beauty_salon.entity.Category;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CategoryResponse {
    private Long id;
    private String name;
    private String salonName;

    public CategoryResponse(Category category) {
        id = category.getId()  ;
        name = category.getName();
        salonName = category.getSalon().getName();
    }
}
