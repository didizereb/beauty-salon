package diana.zherebetska.beauty_salon.dto.response;

import diana.zherebetska.beauty_salon.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserResponse {
    private Long id;
    private  String name;
    private String surname;
    private  Integer phone;
    private String email;

    public  UserResponse (User user){
        id = user.getId();
        name = user.getName();
        surname = user.getSurname();
        phone = user.getPhone();
        email = user.getEmail();

    }
}
