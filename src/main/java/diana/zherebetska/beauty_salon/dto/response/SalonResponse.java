package diana.zherebetska.beauty_salon.dto.response;

import diana.zherebetska.beauty_salon.entity.Salon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SalonResponse {
    private Long id;
    private  String name;
    private  Integer phone;


public SalonResponse (Salon salon){
    id = salon.getId();
    name= salon.getName();
    phone = salon.getPhone();

}
}
