package diana.zherebetska.beauty_salon.dto.response;

import diana.zherebetska.beauty_salon.entity.Record;
import diana.zherebetska.beauty_salon.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class RecordResponse {
    private Date date;
    private Time time;
    private String userName;
    private List<ServiceResponse> servises;

 public RecordResponse (Record record){
        date = record.getDate();
        time = record.getTime();
        userName = record.getUser().getName();
        servises = record.getServices().stream().map(ServiceResponse::new).collect(Collectors.toList());
    }
}
