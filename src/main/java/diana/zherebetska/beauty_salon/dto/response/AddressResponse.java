package diana.zherebetska.beauty_salon.dto.response;

import diana.zherebetska.beauty_salon.entity.Address;
import diana.zherebetska.beauty_salon.entity.Salon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class AddressResponse {
    private Long id;
    private  String country;
    private String city;
    private String street;
    private  Integer building;
    private  Salon salon;

    public AddressResponse (Address address){
        id = address.getId();
        country = address.getCountry();
        city = address.getCity();
        street = address.getStreet();
        building = address.getBuilding();
        salon = getSalon();
    }
}
