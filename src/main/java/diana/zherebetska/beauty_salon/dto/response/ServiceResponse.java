package diana.zherebetska.beauty_salon.dto.response;

import diana.zherebetska.beauty_salon.entity.Category;
import diana.zherebetska.beauty_salon.entity.Record;
import diana.zherebetska.beauty_salon.entity.Salon;
import diana.zherebetska.beauty_salon.entity.Service;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ServiceResponse {
    private Long id;
    private  String name;
    private  Integer price;
    private String categoryName;

    public ServiceResponse (Service service){
        id = service.getId();
        name = service.getName();
        price = service.getPrice();
        categoryName = service.getCategory().getName();
    }
}
