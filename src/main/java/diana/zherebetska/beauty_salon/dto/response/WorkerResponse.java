package diana.zherebetska.beauty_salon.dto.response;

import diana.zherebetska.beauty_salon.entity.Worker;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WorkerResponse {
    private Long id;
    private  String name;
    private String surname;
    private  String phone;
    private  String photo;
    private  Integer experience;
    private String serviceName;
    private String salonName;
    private Long serviceId;
    private Long salonId;

    public WorkerResponse (Worker worker){
        id = worker.getId();
        name= worker.getName();
        surname=worker.getSurname();
        phone= worker.getPhone();
        photo=worker.getPhoto();
        experience = worker.getExperience();
        serviceName = worker.getService().getName();
        salonName = worker.getSalon().getName();
        serviceId = worker.getService().getId();
        salonId = worker.getSalon().getId();

    }
}
