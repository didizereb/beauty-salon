package diana.zherebetska.beauty_salon.dto.request;


import diana.zherebetska.beauty_salon.entity.Salon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddressRequest {
    private  String country;
    private String city;
    private String street;
    private  Integer building;

}
