package diana.zherebetska.beauty_salon.dto.request;

import diana.zherebetska.beauty_salon.entity.Address;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SalonRequest {
    private String name;
    private Integer phone;
    private Address address;

}
