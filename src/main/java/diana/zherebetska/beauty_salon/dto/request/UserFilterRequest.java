package diana.zherebetska.beauty_salon.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class UserFilterRequest {

    private String surname;

    private  Integer phone;

    private String email;

    private List<Long> recordsId = new ArrayList<>();

    private PaginationRequest paginationRequest;
}
