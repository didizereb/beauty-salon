package diana.zherebetska.beauty_salon.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserRequest {
    private  String name;
    private String surname;
    private  Integer phone;
    private String email;
    private String password;
}
