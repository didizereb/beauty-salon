package diana.zherebetska.beauty_salon.dto.request;

import diana.zherebetska.beauty_salon.entity.Category;
import diana.zherebetska.beauty_salon.entity.Record;
import diana.zherebetska.beauty_salon.entity.Salon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ServiceRequest {
    private  String name;
    private  Integer price;
    private Long categoryId;

}
