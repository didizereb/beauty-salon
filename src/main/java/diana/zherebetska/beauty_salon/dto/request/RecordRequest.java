package diana.zherebetska.beauty_salon.dto.request;

import diana.zherebetska.beauty_salon.entity.Record;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class RecordRequest {
    private Date date;
    private Time time;
    private Long userId;
    private List<Long> servicesId;
}
