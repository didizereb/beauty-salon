package diana.zherebetska.beauty_salon.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WorkerRequest {
    private  String name;
    private String surname;
    private  String phone;
    private  Integer experience;
    private String photo;
    private Long serviceId;
    private  Long salonId;
}
