package diana.zherebetska.beauty_salon.dto.request;

import diana.zherebetska.beauty_salon.entity.Category;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class ServiceFilterRequest {
    private  String name;

    private  Integer priceFrom;

    private  Integer priceTo;

    private List<Long> categoriesId = new ArrayList<>();

    private PaginationRequest pagination;

}
