package diana.zherebetska.beauty_salon.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "_service")
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private  String name;

    private  Integer price;

    @OneToMany(mappedBy = "service")
    List<Worker> workers = new ArrayList<>();

    @ManyToMany(mappedBy = "services")
     List <Record> records = new ArrayList<>();

    @ManyToOne
    private Category category;




}

