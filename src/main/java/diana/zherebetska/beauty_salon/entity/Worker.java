package diana.zherebetska.beauty_salon.entity;



import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
//@NamedQuery(name = "Worker.findByExperience", query = "select w from Worker w where experience between ?1 and ?2")

public class Worker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @NotBlank
    private  String name;
    @NotNull
    @NotBlank
    private String surname;
    private  String phone;
    private  Integer experience;

    private String photo;

   @ManyToOne
   private Service service;

   @ManyToOne
    private Salon salon;


}
