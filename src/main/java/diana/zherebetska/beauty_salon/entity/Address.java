package diana.zherebetska.beauty_salon.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.catalina.LifecycleState;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String country;
    private String city;
    private String street;
    private  Integer building;

    @OneToMany(mappedBy = "address")
    List<Salon> salons = new ArrayList<>();


}
