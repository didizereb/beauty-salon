package diana.zherebetska.beauty_salon.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
@NotNull
@NotBlank
    private  String name;
    @NotNull
    @NotBlank
    private String surname;

    private  Integer phone;
    @Email
    private String email;
    @Min(6)
    private String password;

    @OneToMany(mappedBy = "user")
    List<Record> records = new ArrayList<>();


}

