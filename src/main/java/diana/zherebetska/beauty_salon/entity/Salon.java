package diana.zherebetska.beauty_salon.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Salon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String name;
    private Integer phone;

   @OneToMany(mappedBy = "salon")
   private List<Category> categories = new ArrayList<>();

   @ManyToOne
   private Address address;

   @OneToMany(mappedBy = "salon")
  List<Worker> workers = new ArrayList<>();


}
