package diana.zherebetska.beauty_salon.controller;

import diana.zherebetska.beauty_salon.dto.request.WorkerRequest;
import diana.zherebetska.beauty_salon.dto.response.WorkerResponse;
import diana.zherebetska.beauty_salon.entity.Worker;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/worker")
public class WorkerController {
    @Autowired
     private  WorkerService workerService;

    @PostMapping
    public WorkerResponse save (@RequestBody WorkerRequest workerRequest) throws WrongInputException {
        return workerService.save(workerRequest);
    }
    @GetMapping
    public List<WorkerResponse> findAll(){
        return workerService.findAll();
    }


    @GetMapping("/one")
    public WorkerResponse findOne (@RequestParam Long id) throws WrongInputException {
        return workerService.findOneById(id);
    }



    @PutMapping
public WorkerResponse update (@RequestParam Long id, @RequestBody WorkerRequest workerRequest) throws WrongInputException {
        return workerService.update(id,workerRequest);
    }
    @DeleteMapping
    public  void delete (Long id) throws WrongInputException {
   workerService.delete(id);
    }
}
