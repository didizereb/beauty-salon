package diana.zherebetska.beauty_salon.controller;

import diana.zherebetska.beauty_salon.dto.request.CategoryRequest;
import diana.zherebetska.beauty_salon.dto.response.CategoryResponse;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
private  CategoryService categoryService;

    @PutMapping
    public CategoryResponse update (@RequestParam Long id, CategoryRequest categoryRequest) throws WrongInputException {
       return categoryService.update(id,categoryRequest);
    }
    @GetMapping
    public  List<CategoryResponse> findAll(){
        return categoryService.findAll();
    }
    @DeleteMapping
    public  void delete (Long id) throws WrongInputException {
        categoryService.delete(id);
    }
    @PostMapping
    public CategoryResponse save (@RequestBody CategoryRequest categoryRequest) throws WrongInputException {
       return categoryService.save(categoryRequest);
    }



}
