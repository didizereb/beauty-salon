package diana.zherebetska.beauty_salon.controller;

import diana.zherebetska.beauty_salon.dto.request.RecordRequest;
import diana.zherebetska.beauty_salon.dto.response.RecordResponse;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/record")
public class RecordController {
    @Autowired
    RecordService recordService;
    @PutMapping
    public RecordResponse update (@RequestParam Long id, RecordRequest recordRequest) throws WrongInputException {
        return recordService.update( recordRequest, id);
    }
    @GetMapping
    public List<RecordResponse> findAll(){
        return recordService.findAll();
    }
    @DeleteMapping
    public  void delete (Long id) throws WrongInputException {
        recordService.delete(id);
    }
    @PostMapping
    public RecordResponse save (@RequestBody RecordRequest recordRequest) throws WrongInputException {
        return recordService.save(recordRequest);
    }

}
