package diana.zherebetska.beauty_salon.controller;

import diana.zherebetska.beauty_salon.dto.request.AddressRequest;
import diana.zherebetska.beauty_salon.dto.response.AddressResponse;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/address")
public class AddressController {
    @Autowired
private AddressService addressService;


@PostMapping
    public AddressResponse save (@RequestBody AddressRequest addressRequest){
    return addressService.save(addressRequest);
}
@GetMapping
public List<AddressResponse> findAll (){
   return addressService.findAll();
}

@PutMapping
    public AddressResponse update (@RequestParam Long id, @RequestBody  AddressRequest addressRequest) throws WrongInputException {
    return addressService.update(id,addressRequest);
}
@DeleteMapping
    public void delete (Long id) throws WrongInputException {
    addressService.delete(id);
}
}
