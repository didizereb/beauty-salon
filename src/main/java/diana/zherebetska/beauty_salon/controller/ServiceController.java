package diana.zherebetska.beauty_salon.controller;

import diana.zherebetska.beauty_salon.dto.request.AddressRequest;
import diana.zherebetska.beauty_salon.dto.request.ServiceFilterRequest;
import diana.zherebetska.beauty_salon.dto.request.ServiceRequest;
import diana.zherebetska.beauty_salon.dto.response.AddressResponse;
import diana.zherebetska.beauty_salon.dto.response.DataResponse;
import diana.zherebetska.beauty_salon.dto.response.ServiceResponse;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/service")
public class ServiceController {

    @Autowired
    ServiceService serviceService;
    @PostMapping
    public ServiceResponse save (@RequestBody ServiceRequest serviceRequest ) throws WrongInputException {
        return serviceService.save(serviceRequest);
    }
    @GetMapping
    public List<ServiceResponse> findAll (){
        return serviceService.findAll();
    }

    @GetMapping ("/one")
    public ServiceResponse findOne (@RequestParam Long id) throws WrongInputException {
        return serviceService.findOneById(id);
    }
    @PutMapping
    public ServiceResponse update (@RequestParam Long id, @RequestBody  ServiceRequest serviceRequest) throws WrongInputException {
        return serviceService.update(id,serviceRequest);
    }

    @PostMapping("/filter")
    public DataResponse<ServiceResponse> findAllByFilter (@RequestBody ServiceFilterRequest serviceFilterRequest){
      return   serviceService.findByFilter(serviceFilterRequest);
    }

    @DeleteMapping
    public void delete (Long id) throws WrongInputException {
        serviceService.delete(id);
    }
}
