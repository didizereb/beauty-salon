package diana.zherebetska.beauty_salon.controller;

import diana.zherebetska.beauty_salon.dto.request.SalonRequest;
import diana.zherebetska.beauty_salon.dto.response.SalonResponse;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.service.SalonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/salon")
public class SalonController {
    @Autowired
    private SalonService salonService;

    @PostMapping
    public SalonResponse save (@RequestBody SalonRequest salonRequest) {
        return salonService.save(salonRequest);
    }
    @GetMapping
    public List<SalonResponse> findAll(){
      return   salonService.findAll();
    }
    @PutMapping
    public SalonResponse update (@RequestParam Long id, @RequestBody SalonRequest salonRequest) throws WrongInputException {
    return salonService.update(id,salonRequest);
    }
    @DeleteMapping
    public void delete (Long id) throws WrongInputException {
    salonService.delete(id);
    }
}
