package diana.zherebetska.beauty_salon.controller;

import diana.zherebetska.beauty_salon.dto.request.PaginationRequest;
import diana.zherebetska.beauty_salon.dto.request.UserFilterRequest;
import diana.zherebetska.beauty_salon.dto.request.UserRequest;
import diana.zherebetska.beauty_salon.dto.response.DataResponse;
import diana.zherebetska.beauty_salon.dto.response.UserResponse;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
   private UserService userService;

    @PostMapping
    public UserResponse save(@RequestBody UserRequest userRequest){
        return userService.save(userRequest);
    }
    @GetMapping
    public List<UserResponse> findAll (){
        return userService.findAll();
    }
    @PutMapping
    public UserResponse update (@RequestParam Long id, @RequestBody UserRequest userRequest) throws WrongInputException {
        return userService.update(id,userRequest);
    }

    @PostMapping("/page")
    DataResponse<UserResponse> findAll  (@RequestBody PaginationRequest paginationRequest){
        return userService.findAll(paginationRequest);
    }

    @PostMapping("/filter")
    public DataResponse<UserResponse> findByFilter (@RequestBody UserFilterRequest userRequest){
        return userService.findAllByFilter(userRequest);
    }


    @DeleteMapping
    public void delete (Long id) throws WrongInputException {
        userService.delete(id);
    }
}
