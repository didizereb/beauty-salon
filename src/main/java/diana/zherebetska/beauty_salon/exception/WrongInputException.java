package diana.zherebetska.beauty_salon.exception;

public class WrongInputException extends Exception {
    public WrongInputException() {
    }

    public WrongInputException(String message) {
        super(message);
    }
}
