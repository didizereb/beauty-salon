package diana.zherebetska.beauty_salon.specification;

import diana.zherebetska.beauty_salon.dto.request.UserFilterRequest;
import diana.zherebetska.beauty_salon.entity.Category;
import diana.zherebetska.beauty_salon.entity.Record;
import diana.zherebetska.beauty_salon.entity.Service;
import diana.zherebetska.beauty_salon.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class UserSpecification implements Specification<User> {

    private UserFilterRequest filter;


    public UserSpecification(UserFilterRequest filter) {
        this.filter = filter;
    }
    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        Predicate byRecord = findByRecord(root,criteriaBuilder);

        Predicate bySurnameLike =findBySurnameLike(root,criteriaBuilder);
        if (bySurnameLike != null) predicates.add(bySurnameLike);

        Predicate byPhoneLike = findByPhone(root,criteriaBuilder);
        if (byPhoneLike != null) predicates.add(byPhoneLike);

        Predicate byEmailLike = findByEmail(root,criteriaBuilder);
        if (byEmailLike != null)predicates.add(byEmailLike);


        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));

    }
    private  Predicate findByRecord (Root<User> root, CriteriaBuilder cb) {
        Join<User, Record> recordJoin = root.join("record");
        return recordJoin.get("id").in(filter.getRecordsId().toArray());
    }

    private Predicate findBySurnameLike(Root<User> r, CriteriaBuilder cb) {
        String surname = filter.getSurname();
        if (surname == null || surname.trim().isEmpty()) {
            return null;
        }
        return cb.like(r.get("surname"), '%' + surname + '%');
    }

    private Predicate findByPhone(Root<User> r, CriteriaBuilder cb) {
        Integer phone = filter.getPhone();
        String phone1= phone.toString();
        if (filter.getPhone() == null ) {
            return null;
        }
        return cb.like(r.get("phone"), '&' + phone1 + '&');

    }
    private Predicate findByEmail(Root<User> r, CriteriaBuilder cb) {
        String email = filter.getEmail();

        if (filter.getPhone() == null ) {
            return null;
        }
        return cb.like(r.get("phone"), '&' + email + '&');

    }



}