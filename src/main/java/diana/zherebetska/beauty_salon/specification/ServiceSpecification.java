package diana.zherebetska.beauty_salon.specification;

import diana.zherebetska.beauty_salon.dto.request.ServiceFilterRequest;
import diana.zherebetska.beauty_salon.entity.Category;
import diana.zherebetska.beauty_salon.entity.Service;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class ServiceSpecification implements Specification<Service> {
    private ServiceFilterRequest filterRequest;

    public ServiceSpecification(ServiceFilterRequest filterRequest) {
        this.filterRequest = filterRequest;
    }

    @Override
    public Predicate toPredicate(Root<Service> r, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<>();

        Predicate byYear = findByYear(r, cb);
        if (byYear != null) predicates.add(byYear);

        Predicate byNameLike = findByNameLike(r, cb);
        if (byNameLike != null) predicates.add(byNameLike);

        Predicate byCountry = findByCategory(r, cb);
        if (byCountry != null) predicates.add(byCountry);

        return cb.and(predicates.toArray(new Predicate[0]));
    }


    private Predicate findByYear(Root<Service> root, CriteriaBuilder criteriaBuilder) {
        if (filterRequest.getPriceFrom() == null || filterRequest.getPriceTo() == null) {
            return null;
        }
        if (filterRequest.getPriceFrom() == null) {
            filterRequest.setPriceFrom(0);
        }
        if (filterRequest.getPriceTo() == null) {
            filterRequest.setPriceTo(Integer.MAX_VALUE);
        }
       return criteriaBuilder.between(root.get("price"), filterRequest.getPriceFrom(), filterRequest.getPriceTo());
    }
    private Predicate findByCategory(Root<Service> r, CriteriaBuilder cb) {
        Join<Service, Category> categoryJoin = r.join("category");
        return categoryJoin.get("id").in(filterRequest.getCategoriesId().toArray());
    }

    private Predicate findByNameLike(Root<Service> r, CriteriaBuilder cb) {
        String name = filterRequest.getName();
        if (name == null || name.trim().isEmpty()) {
            return null;
        }
        return cb.like(r.get("name"), '%' + name + '%');
    }


}
