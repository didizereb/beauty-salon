package diana.zherebetska.beauty_salon.repository;

import diana.zherebetska.beauty_salon.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends JpaRepository <Service, Long>, JpaSpecificationExecutor<Service> {
}
