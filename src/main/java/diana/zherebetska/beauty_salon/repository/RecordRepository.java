package diana.zherebetska.beauty_salon.repository;

import diana.zherebetska.beauty_salon.entity.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RecordRepository extends JpaRepository <Record, Long> {
}
