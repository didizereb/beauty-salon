package diana.zherebetska.beauty_salon.repository;


import diana.zherebetska.beauty_salon.entity.Salon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalonRepository extends JpaRepository<Salon, Long> {
}
