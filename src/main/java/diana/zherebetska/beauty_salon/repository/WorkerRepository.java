package diana.zherebetska.beauty_salon.repository;


import diana.zherebetska.beauty_salon.entity.Worker;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface WorkerRepository extends JpaRepository<Worker,Long> {
//    List<Worker> findByExperience (Integer from, Integer to);
}
