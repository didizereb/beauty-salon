package diana.zherebetska.beauty_salon;

import diana.zherebetska.beauty_salon.entity.*;
import diana.zherebetska.beauty_salon.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class BeautySalonApplication {

    @Autowired
    private WorkerRepository workerRepository ;
    @Autowired
    private SalonRepository salonRepository;
    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired RecordRepository recordRepository;


    @PostConstruct
    private void init (){
//        Worker worker1 = new Worker();
//        Salon salon1 = new Salon();
//        Service service1 = new Service();
//        Category category1 = new Category();
//        Address address = new Address();
//        Record record = new Record();
//        User user = new User();
//        Salon salon = new Salon();
//
//        salon.setName("PrettyWoman(Franka)");
//        salonRepository.save(salon);
//
//        category.setName("Nails");
//        categoryRepository.save(category);
//
//        service.setName("Манікюр");
//        service.setCategory(category);
//        service.setPrice(200);
//        serviceRepository.save(service);
//
//worker1.setName("Alla");
//worker1.setSurname("Doskich");
//worker1.setExperience(2);
//worker1.setPhone("38099287614");
//worker1.setSalon(salon);
//worker1.setService(service);
//
//        salon1.setName("PrettyWoman(Doroshenka)");
//        salonRepository.save(salon1);
//
//        category1.setName("Make-up");
//        categoryRepository.save(category1);
//
//        service1.setName("smokey-eyes");
//        service1.setCategory(category1);
//        service1.setPrice(280);
//        serviceRepository.save(service1);
//        Worker worker2 = new Worker();
//        worker2.setName("Kate");
//        worker2.setSurname("Melnyk");
//        worker2.setExperience(5);
//        worker2.setPhone("3809329854");
//        worker2.setSalon(salon);
//        worker2.setService(service1);
//
//workerRepository.save(worker2);
//workerRepository.findByExperience(2,5).forEach(System.out::println);



    }

    public static void main(String[] args) {
        SpringApplication.run(BeautySalonApplication.class, args);
    }

}

