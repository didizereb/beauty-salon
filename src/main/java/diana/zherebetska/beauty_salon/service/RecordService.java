package diana.zherebetska.beauty_salon.service;

import diana.zherebetska.beauty_salon.dto.request.RecordRequest;
import diana.zherebetska.beauty_salon.dto.response.RecordResponse;
import diana.zherebetska.beauty_salon.entity.Record;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.repository.RecordRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import diana.zherebetska.beauty_salon.entity.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class RecordService {

    @Autowired
    RecordRepository recordRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private ServiceService serviceService;

    public List<RecordResponse> findAll (){
        return recordRepository.findAll().stream().map(RecordResponse::new).collect(Collectors.toList());
    }

    public Record findOne (Long id) throws WrongInputException {
        return  recordRepository.findById(id).orElseThrow(() -> new WrongInputException("Record with id" + id + "not exist"));
    }

    @SneakyThrows
    private Record recordRequestToRecord(RecordRequest request, Record record ) {
        if (record == null) {
            record = new Record();
        }
        record.setDate(request.getDate());
        record.setTime(request.getTime());
        record.setUser(userService.findOne(request.getUserId()));
        record = recordRepository.save(record);

        List<Service> services = new ArrayList<>();
//        request.getServicesId().forEach(id -> services.add(serviceService.findOne(id)));
        for (Long id : request.getServicesId()) {
            services.add(serviceService.findOne(id));
        }
        record.setServices(services);
        return recordRepository.save(record);
    }
    public RecordResponse save(RecordRequest recordRequest) throws WrongInputException {
        return  new RecordResponse(recordRequestToRecord(recordRequest,null));
    }

    public RecordResponse update (RecordRequest recordRequest,Long id) throws WrongInputException {
        return new RecordResponse(recordRequestToRecord(recordRequest, findOne(id)));
}

    public  void  delete (Long id) throws WrongInputException {
        recordRepository.delete(findOne(id));
}

}
