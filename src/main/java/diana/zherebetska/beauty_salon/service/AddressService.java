package diana.zherebetska.beauty_salon.service;

import diana.zherebetska.beauty_salon.dto.request.AddressRequest;
import diana.zherebetska.beauty_salon.dto.response.AddressResponse;
import diana.zherebetska.beauty_salon.entity.Address;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddressService {

    @Autowired
   private AddressRepository addressRepository;


    public AddressResponse save(AddressRequest addressRequest){
//        Address address = addressRepository.save(addressRequestToAddress(null,addressRequest))
        return new AddressResponse(addressRepository.save(addressRequestToAddress(null,addressRequest)));

//        Address address = new Address();
//        address.setCountry(addressRequest.getCountry());
//        address.setCity(addressRequest.getCity());
//        address.setStreet(addressRequest.getStreet());
//        address.setBuilding(addressRequest.getBuilding());
//        Address saved = addressRepository.save(address);
//        return new AddressResponse(saved);
    }

    public AddressResponse update (Long id, AddressRequest addressRequest ) throws WrongInputException {
        return new AddressResponse(addressRepository.save(addressRequestToAddress(findOne(id),addressRequest)));
    }


    private Address addressRequestToAddress (Address address ,AddressRequest addressRequest ){
        if (address == null){
            address = new Address();
        }
        address.setCountry(addressRequest.getCountry());
        address.setCity(addressRequest.getCity());
        address.setStreet(addressRequest.getStreet());
        address.setBuilding(addressRequest.getBuilding());
        return address;
    }

    public Address findOne (Long id) throws WrongInputException {
        return addressRepository.findById(id).orElseThrow( () -> new WrongInputException("Address with id" + id + "not exists"));
    }

    public List<AddressResponse> findAll(){
     return addressRepository.findAll().stream().map(AddressResponse::new).collect(Collectors.toList());
    }

    public  void delete (Long id) throws WrongInputException {
        addressRepository.delete(findOne(id));
    }

}
