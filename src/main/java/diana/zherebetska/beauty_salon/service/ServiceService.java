package diana.zherebetska.beauty_salon.service;

import diana.zherebetska.beauty_salon.dto.request.ServiceFilterRequest;
import diana.zherebetska.beauty_salon.dto.request.ServiceRequest;
import diana.zherebetska.beauty_salon.dto.response.DataResponse;
import diana.zherebetska.beauty_salon.dto.response.ServiceResponse;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.repository.ServiceRepository;
import diana.zherebetska.beauty_salon.repository.UserRepository;
import diana.zherebetska.beauty_salon.specification.ServiceSpecification;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
import diana.zherebetska.beauty_salon.entity.Service;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class ServiceService {

    @Autowired
    ServiceRepository serviceRepository;

    @Autowired
    SalonService salonService;

    @Autowired
    CategoryService categoryService;
    @Autowired
    UserRepository userRepository;

    public ServiceResponse save(ServiceRequest serviceRequest) throws WrongInputException {
        return new ServiceResponse((serviceRequestToService(null, serviceRequest)));
    }

    public Service findOne(Long id) throws WrongInputException {
        return serviceRepository.findById(id).orElseThrow(() -> new WrongInputException("Service with id " + id + " not exists"));
    }

    public ServiceResponse findOneById (Long id) throws WrongInputException {
        return  new ServiceResponse(findOne(id));

    }

    public List<ServiceResponse> findAll() {
        return serviceRepository.findAll().stream().map(ServiceResponse::new).collect(Collectors.toList());

    }

    public Service serviceRequestToService(Service service, ServiceRequest serviceRequest) throws WrongInputException {
        if (service == null) {
            service = new Service();
        }
        service.setCategory(categoryService.findOne(serviceRequest.getCategoryId()));
        service.setName(serviceRequest.getName());
        service.setPrice(serviceRequest.getPrice());

        return serviceRepository.save(service);
    }

    public ServiceResponse update(Long id, ServiceRequest serviceRequest) throws WrongInputException {
        return new ServiceResponse((serviceRequestToService(findOne(id), serviceRequest)));
    }

    public  void delete (Long id) throws WrongInputException {

        serviceRepository.delete(findOne(id));
    }
    public DataResponse<ServiceResponse> findByFilter(ServiceFilterRequest serviceFilterRequest ) {
        Page<Service> page = serviceRepository.findAll(
                new ServiceSpecification(serviceFilterRequest),
                serviceFilterRequest.getPagination().mapToPageRequest());

        return new DataResponse<>(page.get().map(ServiceResponse::new).collect(Collectors.toList()), page.getTotalPages(), page.getTotalElements());

    }

}

