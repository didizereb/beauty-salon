package diana.zherebetska.beauty_salon.service;

import diana.zherebetska.beauty_salon.dto.request.SalonRequest;
import diana.zherebetska.beauty_salon.dto.response.SalonResponse;
import diana.zherebetska.beauty_salon.entity.Salon;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.repository.SalonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SalonService {

@Autowired
private  SalonRepository salonRepository;

public SalonResponse save (SalonRequest salonRequest){
    Salon salon = new Salon();
    salon.setName(salonRequest.getName());
    salon.setPhone(salonRequest.getPhone());
    Salon saved = salonRepository.save(salon);
    return new  SalonResponse(saved);

}
public SalonResponse update (Long id, SalonRequest salonRequest) throws WrongInputException {
    return new SalonResponse(salonRepository.save(salonRequestToSalon(findOne(id),salonRequest)));
}

 public  List<SalonResponse> findAll(){
    return salonRepository.findAll().stream().map(SalonResponse::new).collect(Collectors.toList());
 }

 public Salon findOne (Long id) throws WrongInputException {
    return salonRepository.findById(id).orElseThrow(() ->new  WrongInputException("Salon with id" + id + "not exist"));

 }

 public Salon salonRequestToSalon (Salon salon, SalonRequest request){
    if (salon == null){
        salon = new  Salon();
    }
    salon.setName(request.getName());
    salon.setPhone(request.getPhone());
    return salon;
 }
 public void delete (Long id) throws WrongInputException {
    salonRepository.delete(findOne(id));
 }

}
