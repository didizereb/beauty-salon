package diana.zherebetska.beauty_salon.service;

import diana.zherebetska.beauty_salon.dto.request.PaginationRequest;
import diana.zherebetska.beauty_salon.dto.request.UserFilterRequest;
import diana.zherebetska.beauty_salon.dto.request.UserRequest;
import diana.zherebetska.beauty_salon.dto.response.DataResponse;
import diana.zherebetska.beauty_salon.dto.response.UserResponse;
import diana.zherebetska.beauty_salon.entity.User;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.repository.UserRepository;
import diana.zherebetska.beauty_salon.specification.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public  List<UserResponse> findAll (){
        return userRepository.findAll().stream().map(UserResponse::new).collect(Collectors.toList());
    }

    public User findOne (Long id) throws WrongInputException {
        return userRepository.findById(id).orElseThrow(()->new WrongInputException("User with id" + id + "not exist"));
    }

    public User userRequestToUser (User user, UserRequest userRequest) {

        if (user == null) {
            user = new User();
        }
        user.setName(userRequest.getName());
        user.setSurname(userRequest.getSurname());
        user.setEmail(userRequest.getEmail());
        user.setPhone(userRequest.getPhone());
        user.setPassword(userRequest.getPassword());
        return userRepository.save(user);
    }

    public UserResponse save (UserRequest userRequest){
        return new UserResponse(userRequestToUser(null,userRequest));
    }

public UserResponse update (Long id, UserRequest userRequest) throws WrongInputException {
        return new UserResponse(userRequestToUser(findOne(id),userRequest));
}
public void  delete (Long id) throws WrongInputException {
        userRepository.delete(findOne(id));
}
    public DataResponse<UserResponse> findAll(PaginationRequest pagination) {
        Page<User> all = userRepository.findAll(pagination.mapToPageRequest());
        return new DataResponse<>(all.get().map(UserResponse::new).collect(Collectors.toList()), all.getTotalPages(), all.getTotalElements());
    }

    public DataResponse<UserResponse> findAllByFilter(UserFilterRequest request) {
        Page<User> page = userRepository.findAll(
                new UserSpecification(request),
                request.getPaginationRequest().mapToPageRequest()
        );
        return new DataResponse<>(page.get().map(UserResponse::new).collect(Collectors.toList()),
                page.getTotalPages(), page.getTotalElements());
    }
}
