package diana.zherebetska.beauty_salon.service;

import diana.zherebetska.beauty_salon.dto.request.ServiceRequest;
import diana.zherebetska.beauty_salon.dto.request.WorkerRequest;
import diana.zherebetska.beauty_salon.dto.response.WorkerResponse;
import diana.zherebetska.beauty_salon.entity.Worker;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkerService {

    @Autowired
    WorkerRepository workerRepository;

    @Autowired
    private SalonService salonService;
    @Autowired
    private ServiceService serviceService;


public  WorkerResponse save (WorkerRequest workerRequest) throws WrongInputException {
    return  new WorkerResponse((workerRequestToWorker(null,workerRequest)));
}

    public WorkerResponse update (Long id, WorkerRequest workerRequest) throws WrongInputException {
        return new WorkerResponse((workerRequestToWorker(findOne(id),workerRequest)));
    }

public List<WorkerResponse> findAll (){
    return workerRepository.findAll().stream().map(WorkerResponse::new).collect(Collectors.toList());
}

    public void delete(Long id) throws WrongInputException {
        workerRepository.delete(findOne(id));
    }

    public Worker findOne (Long id) throws WrongInputException {
        return workerRepository.findById(id).orElseThrow(()-> new WrongInputException("Worker with id " + id + " not exists"));
    }
    public WorkerResponse findOneById(Long id) throws WrongInputException {
        return new WorkerResponse(findOne(id));
    }

private Worker workerRequestToWorker (Worker worker, WorkerRequest workerRequest) throws WrongInputException {
        if (worker == null){
            worker = new Worker();
        }
        worker.setSalon(salonService.findOne(workerRequest.getSalonId()));
        worker.setService(serviceService.findOne(workerRequest.getServiceId()));
        worker.setName(workerRequest.getName());
        worker.setSurname(workerRequest.getSurname());
        if (workerRequest.getPhoto() != ""){
            worker.setPhoto(workerRequest.getPhoto());

        }

        worker.setPhone(workerRequest.getPhone());
        worker.setExperience(workerRequest.getExperience());
    return workerRepository.save(worker);
}

}
