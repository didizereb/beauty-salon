package diana.zherebetska.beauty_salon.service;

import diana.zherebetska.beauty_salon.dto.request.AddressRequest;
import diana.zherebetska.beauty_salon.dto.request.CategoryRequest;
import diana.zherebetska.beauty_salon.dto.response.AddressResponse;
import diana.zherebetska.beauty_salon.dto.response.CategoryResponse;
import diana.zherebetska.beauty_salon.entity.Category;
import diana.zherebetska.beauty_salon.exception.WrongInputException;
import diana.zherebetska.beauty_salon.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SalonService salonService;


public CategoryResponse save (CategoryRequest categoryRequest) throws WrongInputException {
    return new CategoryResponse(categoryRepository.save(categoryRequestToCategory(null, categoryRequest)));
}

    public List<CategoryResponse> findAll(){
    return  categoryRepository.findAll().stream().map(CategoryResponse::new).collect(Collectors.toList());

    }

    public CategoryResponse update (Long id, CategoryRequest categoryRequest ) throws WrongInputException {
        return new CategoryResponse(categoryRepository.save(categoryRequestToCategory(findOne(id),categoryRequest)));

    }

    private Category categoryRequestToCategory (Category category ,CategoryRequest categoryRequest ) throws WrongInputException {
        if (category == null){
            category = new Category();
        }
        category.setName(categoryRequest.getName());
        category.setSalon(salonService.findOne(categoryRequest.getSalonId()));
        return category;
    }
    public Category findOne (Long id) throws WrongInputException {
      return categoryRepository.findById(id).orElseThrow(() -> new WrongInputException("Categoru with id" + id + "not exists"));
    }


    public  void delete (Long id) throws WrongInputException {
        categoryRepository.delete(findOne(id));
    }


}

